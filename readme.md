# Serverless-Newman

如果想自動測試你的務 API 接口  
可透過本專案的 Newman 工具, 自動打 api 服務  
目前有 2 種方式 : 

# 直接透過 GitLab CICD (docker) 測試 API, 並發佈到 GitLab Page

 - Update Postman json api file to `collections/example.json`
 - Commit and Push code to gitlab
 - Gitlab will auto run CICD and use `newman` command to test your api.
 - Api test results will update and show on [https://scottchayaa-public.gitlab.io/serverless-newman](https://scottchayaa-public.gitlab.io/serverless-newman)

![](images/run-on-docker-result.png)

# 半自動執行 Lambda 透過 GitLab CICD, 並將結果存至 S3

 - GitLab CICD Variables 需配置 `AWS_ACCESS_KEY_ID` 和 `AWS_SECRET_ACCESS_KEY` 
 - Create `s3 buckect` on aws
 - Confirm and update `.env`,
 - Export postman collection and Update to `collections/example.json`
 - Commit and Push code to gitlab
 - Gitlab CICD will deploy sls code to aws lambda and trigger lambda funtion by CICD.
 - Api test results will send to s3 buckect path which you defined on `.env`.

![](images/aws-lambda-result.png)

# 本機使用 serverless 部屬

```sh
# 先確認 awscli 有設定好
aws configure list

# Install serverless tool
npm install serverless -g

# Install app dependencies
yarn install

# deploy to aws lambda
serverless deploy -v

# Run lambda function by manual
serverless invoke -f exportCsv
serverless invoke -f exportHtml

# print logs from cloudwatch
serverless logs -f ${lambda_function_name}
# tail logs from cloudwatch continutely
serverless logs -f exportCsv -t
serverless logs -f exportHtml -t

# Remove serverless app
serverless remove
```
