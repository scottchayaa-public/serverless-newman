'use strict';

require('dotenv').config();

const newman = require('newman');
const AWS = require('aws-sdk');
const s3 = new AWS.S3();
const fs = require('fs');
const dateFormat = require("dateformat");

let COLLECTION_FILE = process.env.COLLECTION_FILE;
let DEFAULT_EXPORT_PATH = '/tmp/result'; // lambda 環境除了 /tmp, 其他地方都是 read-only
let BUCKET = process.env.AWS_BUCKET;
let EXPORT_DIR = process.env.AWS_BUCKET_EXPORT_NEWMAN_DIR;

function runNewman(collection_file, export_type) {
    return new Promise((resolve, reject) => {
        newman.run({
            collection: require(collection_file),
            reporters: export_type,
            reporter: {
                [export_type]: {
                    export: DEFAULT_EXPORT_PATH
                }
            }
        }, function (err) {
            if (err) {
                reject(err);
            }
            resolve();
        });
    });
}

module.exports.exportCsv = async (event) => {
    try {
        await runNewman(COLLECTION_FILE, 'csv');

        let now = dateFormat(new Date(), "yyyymmddhhMMss");

        const destparams = {
            Bucket: BUCKET,
            Key: `${EXPORT_DIR}/${now}.csv`,
            Body: fs.readFileSync(DEFAULT_EXPORT_PATH)
        };

        const putResult = await s3.putObject(destparams).promise();
        console.log('=== newman::exportCsv() complete! ===');
        console.log(putResult);
    } catch (error) {
        console.error(error);
    }
};

module.exports.exportHtml = async (event) => {
    try {
        await runNewman(COLLECTION_FILE, 'htmlextra');

        let now = dateFormat(new Date(), "yyyymmddhhMMss");

        const destparams = {
            Bucket: BUCKET,
            Key: `${EXPORT_DIR}/${now}.html`,
            Body: fs.readFileSync(DEFAULT_EXPORT_PATH)
        };

        const putResult = await s3.putObject(destparams).promise();
        console.log('=== newman::exportHtml() complete! ===');
        console.log(putResult);
    } catch (error) {
        console.error(error);
    }
};